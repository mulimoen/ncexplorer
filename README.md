# NC Explorer

Tool to visualize geo-referenced data from NetCDF files.

![window screenshot](docs/screenshot.png)

## Installation

Either download the binary for Windows and Linux from the [Releases](https://gitlab.com/samoylovfp/ncexplorer/-/releases) page

Or if you have `cargo` installed:

    cargo install nc_explorer

Requires at least rust 1.59

## Usage

Drag-and-drop files you want to plot onto the nc_explorer window, or use "open with" dialog.

Variables that have latitude and longitude in dimensions will have a `Plot` button next to them. Click it to see the data.

## Status

This is in a proof-of-concept stage. Please do not hesitate to create issues if something does not work or if you want to add something.


The code is in a pretty rough shape and will be re-organized, I will be accepting merge requests after that.
