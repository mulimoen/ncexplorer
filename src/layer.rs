use super::file::LoadedNetcdfFile;

use eframe::{
    egui::{
        plot::{PlotImage, PlotUi, Value},
        Pos2, Rect,
    },
    epaint::TextureHandle,
};
use itertools::Itertools;

use poll_promise::Promise;
use std::sync::Arc;

pub struct Layer {
    pub file: Arc<LoadedNetcdfFile>,
    pub variable_name: String,
    pub current_view: Option<LayerView>,
    pub next_view: Option<LayerView>,
    pub visible: bool,
}

impl Layer {
    pub fn draw(&self, plot_ui: &mut PlotUi) {
        if let Some(view) = &self.current_view {
            if let Some(ld) = &view.loaded_data {
                if let std::task::Poll::Ready(Ok(ld)) = ld.poll() {
                    plot_ui.image(PlotImage::new(
                        &ld.texture_id,
                        Value::new(ld.bounds.center().x, ld.bounds.center().y),
                        [ld.bounds.width(), ld.bounds.height()],
                    ));
                }
            }
        }
    }

    pub fn check_if_loaded(&mut self) {
        if let Some(nv) = &self.next_view {
            if let Some(ld) = &nv.loaded_data {
                match ld.poll() {
                    std::task::Poll::Ready(Ok(_)) => {
                        self.current_view = self.next_view.take();
                    }
                    std::task::Poll::Ready(Err(e)) => {
                        println!("{e:?}");
                        self.next_view = None;
                    }
                    std::task::Poll::Pending => {}
                }
            }
        }
    }

    pub fn default_view(&self) -> LayerView {
        let x = self.file.vars.iter().find(|v| v.is_x()).unwrap();
        let y = self.file.vars.iter().find(|v| v.is_y()).unwrap();

        let (x_min, x_max) = x
            .values
            .as_ref()
            .unwrap()
            .iter()
            .minmax()
            .into_option()
            .unwrap();
        let (y_min, y_max) = y
            .values
            .as_ref()
            .unwrap()
            .iter()
            .minmax()
            .into_option()
            .unwrap();

        LayerView {
            loaded_data: None,
            rect: Rect {
                min: Pos2::new(x_min.into_inner(), y_min.into_inner()),
                max: Pos2::new(x_max.into_inner(), y_max.into_inner()),
            },
        }
    }
}

pub struct Values {
    pub row_size: usize,
    pub col_size: usize,
    pub data: Vec<f32>,
}

pub struct LoadedData {
    pub texture_id: TextureHandle,
    pub values: Arc<Values>,
    pub bounds: Rect,
}

pub struct LayerView {
    pub loaded_data: Option<Promise<anyhow::Result<LoadedData>>>,
    // in egui plot coordinates
    pub rect: Rect,
}

// impl Drop for LoadedData {
//     fn drop(&mut self) {
//         self.frame.free_texture(self.texture_id);
//     }
// }
