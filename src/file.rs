use super::{
    layer::{Layer, LoadedData, Values},
    variable::Variable,
};
use anyhow::Context;
use eframe::{
    egui::{self, CollapsingHeader, Color32, Pos2, Rect, RichText},
    epaint::ColorImage,
};
use itertools::{iproduct, Itertools};

use poll_promise::Promise;
use std::{path::PathBuf, sync::Arc, task::Poll};

pub struct OpenNetcdfFile {
    pub path: PathBuf,
    pub loaded_file: Promise<anyhow::Result<Arc<LoadedNetcdfFile>>>,
}

#[derive(Debug)]
pub struct LoadedNetcdfFile {
    pub nc_file: Arc<netcdf::File>,
    pub vars: Vec<Variable>,
}

impl OpenNetcdfFile {
    pub fn render_ui(
        &self,
        ui: &mut egui::Ui,
        layers_to_draw: &mut Vec<Layer>,
        files_to_close: &mut Vec<PathBuf>,
    ) {
        let title = RichText::new(
            self.path
                .file_name()
                .map(|s| s.to_string_lossy().to_string())
                .unwrap_or_else(|| "no file name".to_string()),
        )
        .color(match self.loaded_file.poll() {
            Poll::Ready(Ok(_)) => Color32::WHITE,
            Poll::Ready(Err(_)) => Color32::RED,
            Poll::Pending => Color32::GRAY,
        });
        CollapsingHeader::new(title)
            .default_open(true)
            .show(ui, |ui| {
                match self.loaded_file.poll() {
                    Poll::Ready(Ok(f)) => {
                        for v in &f.vars {
                            v.render_ui(f, ui, layers_to_draw);
                        }
                    }
                    Poll::Ready(Err(e)) => {
                        ui.label(format!("Error {e:?}"));
                    }
                    Poll::Pending => {
                        ui.label("Loading...");
                    }
                }
                if ui.button("close").clicked() {
                    files_to_close.push(self.path.clone())
                }
            });
        ui.separator();
    }
}

pub fn load_file(df: PathBuf) -> anyhow::Result<Arc<LoadedNetcdfFile>> {
    let nc_file = netcdf::open(&df)?;
    let vars = nc_file
        .variables()
        .map(Variable::try_from)
        .collect::<anyhow::Result<Vec<_>>>()?;
    Ok(Arc::new(LoadedNetcdfFile {
        nc_file: Arc::new(nc_file),
        vars,
    }))
}

/// Limitations:
/// only plots floating point values
/// assumes dimension values are evenly spaced and monotonally increase
pub fn load_data_into_texture(
    file: Arc<LoadedNetcdfFile>,
    var_name: &str,
    bounds: Rect,
    context: egui::Context,
) -> Result<LoadedData, anyhow::Error> {
    let var = file
        .nc_file
        .variable(var_name)
        .with_context(|| "Variable not found")?;

    let x_dim_idx = var
        .dimensions()
        .iter()
        .position(|d| matches!(d.name().to_lowercase().as_str(), "lon" | "longitude"))
        .unwrap();
    let y_dim_idx = var
        .dimensions()
        .iter()
        .position(|d| matches!(d.name().to_lowercase().as_str(), "lat" | "latitude"))
        .unwrap();

    let texture_size_limit = 1024;

    let x_var = file
        .vars
        .iter()
        .find(|v| v.name == var.dimensions()[x_dim_idx].name())
        .unwrap();
    let y_var = file
        .vars
        .iter()
        .find(|v| v.name == var.dimensions()[y_dim_idx].name())
        .unwrap();

    let x_values = x_var.values.as_ref().unwrap();
    let y_values = y_var.values.as_ref().unwrap();

    let start_x_idx = x_values
        .binary_search(&bounds.min.x.try_into().unwrap())
        .map_or_else(|v| v, |v| v)
        .min(x_values.len() - 1);

    let end_x_idx = x_values
        .binary_search(&bounds.max.x.try_into().unwrap())
        .map_or_else(|v| v, |v| v)
        .max(start_x_idx + 1)
        .min(x_values.len() - 1);

    let start_y_idx = y_values
        .binary_search(&bounds.min.y.try_into().unwrap())
        .map_or_else(|v| v, |v| v)
        .min(y_values.len() - 1);

    let end_y_idx = y_values
        .binary_search(&bounds.max.y.try_into().unwrap())
        .map_or_else(|v| v, |v| v)
        .max(start_y_idx + 1)
        .min(y_values.len() - 1);

    let x_idx_span = (end_x_idx as isize - start_x_idx as isize).abs() as usize;
    let y_idx_span = (end_y_idx as isize - start_y_idx as isize).abs() as usize;
    let max_size = x_idx_span.max(y_idx_span);
    let stride = (max_size / texture_size_limit).max(1);

    let (row_len, col_len) = [x_idx_span, y_idx_span]
        .iter()
        .map(|&s| (s / stride).max(1))
        .collect_tuple()
        .unwrap();

    let mut indices = vec![0; var.dimensions().len()];
    indices[x_dim_idx] = start_x_idx;
    // Y goes up in plot but down in data
    indices[y_dim_idx] = end_y_idx.min(start_y_idx);

    let mut slices_len = vec![1; var.dimensions().len()];
    slices_len[x_dim_idx] = row_len;
    slices_len[y_dim_idx] = col_len;

    let mut values = vec![0.0; row_len * col_len];
    let mut strides = vec![1_isize; var.dimensions().len()];
    strides[x_dim_idx] = stride as _;
    strides[y_dim_idx] = stride as _;
    var.values_strided_to::<f32>(&mut values, Some(&indices), Some(&slices_len), &strides)?;

    let get_val_at = |x: usize, y: usize| values[x + y * row_len];

    // Y goes up in plot but down in data and in an image, we need to reverse it
    let values_properly_oriented: Vec<f32> = iproduct!(0..col_len, 0..row_len)
        .map(|(y, x)| get_val_at(x, col_len - y - 1))
        .collect();

    let (&min, &max) = values_properly_oriented
        .iter()
        .minmax()
        .into_option()
        .unwrap();

    let pixels = values_properly_oriented
        .iter()
        .flat_map(|&v| {
            let scaled = (v - min) * 255.0 / (max - min);
            [0, scaled as u8, 0, 255]
        })
        .collect_vec();
    let image = ColorImage::from_rgba_unmultiplied([row_len, col_len], &pixels);

    Ok(LoadedData {
        texture_id: context.load_texture("map", image),
        values: Arc::new(Values {
            row_size: row_len,
            col_size: col_len,
            data: values_properly_oriented,
        }),
        bounds: Rect {
            min: Pos2::new(
                x_values[start_x_idx].into_inner(),
                y_values[start_y_idx].into_inner(),
            ),
            max: Pos2::new(
                x_values[end_x_idx].into_inner(),
                y_values[end_y_idx].into_inner(),
            ),
        },
    })
}
