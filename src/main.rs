#![windows_subsystem = "windows"]

use crate::app::NcExplorerApp;
use eframe::{egui::Visuals, epaint::Vec2, NativeOptions};
use std::path::PathBuf;
use tracing_subscriber::EnvFilter;

mod app;
mod file;
mod layer;
mod utils;
mod variable;

fn main() {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    let mut app = NcExplorerApp::new();

    std::env::args()
        .skip(1)
        .for_each(|a| app.load_file_bg(PathBuf::from(a)));

    let options = NativeOptions {
        initial_window_size: Some(Vec2::new(800.0, 600.0)),
        drag_and_drop_support: true,
        ..Default::default()
    };

    eframe::run_native(
        "NC Explorer",
        options,
        Box::new(|cc| {
            cc.egui_ctx.set_visuals(Visuals::dark());
            Box::new(app)
        }),
    );
}
