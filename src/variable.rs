use super::{file::LoadedNetcdfFile, layer::Layer};
use eframe::egui::{self, Color32, Grid};
use ordered_float::NotNan;
use std::sync::Arc;

#[derive(Debug)]
pub struct Attribute {
    name: String,
    value: anyhow::Result<String>,
}

#[derive(Debug)]
pub struct Variable {
    pub name: String,
    pub dims: Vec<String>,
    pub var_type: String,
    pub attrs: Vec<Attribute>,
    /// These are only loaded for lat/lon
    pub values: Option<Vec<NotNan<f32>>>,
}

impl Variable {
    pub fn render_ui(
        &self,
        file: &Arc<LoadedNetcdfFile>,
        ui: &mut egui::Ui,
        layers_to_draw: &mut Vec<Layer>,
    ) {
        ui.horizontal(|ui| {
            ui.collapsing(format!("{}: {}", self.name, self.var_type), |ui| {
                ui.strong("Dimensions");
                ui.horizontal(|ui| {
                    ui.label(self.dims.join(", "));
                });
                ui.separator();
                ui.strong("Attributes");
                Grid::new(&self.name).show(ui, |ui| {
                    for a in &self.attrs {
                        ui.label(&a.name);
                        match &a.value {
                            Ok(v) => {
                                ui.label(v);
                            }
                            Err(e) => {
                                ui.colored_label(Color32::RED, format!("{e}"));
                            }
                        }
                        ui.end_row();
                    }
                })
            });
            if self.dims.len() >= 2 && ui.button("Plot").clicked() {
                layers_to_draw.push(Layer {
                    file: Arc::clone(file),
                    variable_name: self.name.clone(),
                    current_view: None,
                    next_view: None,
                    visible: true,
                });
            }
        });
    }

    pub fn is_x(&self) -> bool {
        matches!(self.name.to_lowercase().as_str(), "lon" | "longitude")
    }

    pub fn is_y(&self) -> bool {
        matches!(self.name.to_lowercase().as_str(), "lat" | "latitude")
    }
}

impl TryFrom<netcdf::Variable<'_>> for Variable {
    type Error = anyhow::Error;
    fn try_from(nc_v: netcdf::Variable) -> anyhow::Result<Self> {
        let mut v = Variable {
            name: nc_v.name(),
            var_type: format!("{:?}", nc_v.vartype()),
            attrs: nc_v
                .attributes()
                .map(|a| Attribute {
                    name: a.name().to_string(),
                    value: a.value().map(|a| format!("{:?}", a)).map_err(Into::into),
                })
                .collect(),
            dims: nc_v.dimensions().iter().map(|d| d.name()).collect(),
            values: None,
        };
        if v.is_x() || v.is_y() {
            v.values = Some(
                nc_v.values::<f32>(None, None)?
                    .into_iter()
                    .map(|f| NotNan::try_from(f).unwrap())
                    .collect(),
            );
        }
        Ok(v)
    }
}
