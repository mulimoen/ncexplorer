MACOS_TARGET="x86_64-apple-darwin"

echo "Building target for platform ${MACOS_TARGET}"
echo

# Add osxcross toolchain to path
export PATH="/osxcross/target/bin:$PATH"

# Use Clang for C/C++ builds
export CC=o64-clang
export CXX=o64-clang++
export CMAKE_LINKER=x86_64-apple-darwin14-clang

mkdir -p .cargo

echo '[target.x86_64-apple-darwin]
linker = "x86_64-apple-darwin14-clang"
ar = "x86_64-apple-darwin14-ar"' >> .cargo/config.toml

cargo build --release --target "${MACOS_TARGET}"

echo
echo Done
